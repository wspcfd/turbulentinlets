#ifndef RANDOMWITHBIT_H
#define RANDOMWITHBIT_H

#include "Random.H"
#include "scalar.H"
#include "vector.H"
#include "tensor.H"
#include "sphericalTensor.H"
#include "symmTensor.H"
#include "List.H"

namespace Foam
{


class RandomWithBit : public Random
{

public:

    RandomWithBit(label);
    
    void bitRandomise(scalar&);
    void bitRandomise(vector&);
    void bitRandomise(sphericalTensor&);
    void bitRandomise(symmTensor&);
    void bitRandomise(tensor&);
};

}

#endif
