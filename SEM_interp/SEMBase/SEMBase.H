/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::SEMBase

Description
    Generate a fluctuating inlet condition by adding a random component
    to a reference (mean) field.
SourceFiles
    SEMBase.C

\*---------------------------------------------------------------------------*/

#ifndef SEMBase_H
#define SEMBase_H

#include "RandomWithBit.H"
#include "fixedValueFvPatchFields.H"
#include "DLPtrList.H"
#include "primitivePatchInterpolation.H"
#include "interpolateXY.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{


class SEMspot;

/*---------------------------------------------------------------------------*\
                     Class turbulentInletSEMFvPatch Declaration
\*---------------------------------------------------------------------------*/

class SEMBase
:
    public fixedValueFvPatchField<vector>
{
    // Protected data

protected:
        RandomWithBit ranGen_;
        Field<vector> meanField_;
        bool Interp_;
        label curTimeIndex_; 
        boundBox semBox_;
        Field<scalar> epsIn_;
        scalar UBulk_;
        Field<symmTensor> RIn_;        
        bool embedded_;
        bool bulkConv_;
        bool correctMass_;
        bool newNormalisation_;
        Field<scalar> maxDelta_;
        Field<vector> sigma_; 
        DLList<SEMspot*> spot_;
        scalar avgWindow_;
        scalar maxSigma_;
        scalar ListLength_;
public:
    // Constructors

        //- Construct from patch and internal field
        SEMBase
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        SEMBase
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given SEMBase
        //  onto a new patch
        SEMBase
        (
            const SEMBase&,
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy
        SEMBase
        (
            const SEMBase&
        );

        //- Construct as copy setting internal field reference
        SEMBase
        (
            const SEMBase&,
            const DimensionedField<vector, volMesh>&
        );



            //- Return the reference field
            const Field<vector>& meanField() const
            {
                return meanField_;
            }

            //- Return reference to the reference field to allow adjustment
            Field<vector>& meanField()
            {
                return meanField_;
            }
            bool Interp()
            {
                return Interp_;
            }

            Field<scalar>& epsIn()
            {
                return epsIn_;
            }

            Field<vector>& sigma()
            {
                return sigma_;
            }

            Field<symmTensor>& RIn()
            {
                return RIn_;
            }

            RandomWithBit& ranGen()
            {
                return ranGen_;
            }

            boundBox semBox()
            {
                return semBox_;
            }

            Field<scalar>& maxDelta()
            {
                return maxDelta_;
            }

            bool bulkConv()
            {
                return bulkConv_;
            }

            bool newNormalisation()
            {
                return newNormalisation_;
            }

            DLList<SEMspot*>& spot()
            {
                return spot_;
            }
        // Mapping functions

            //- Map (and resize as needed) from self given a mapping object
            virtual void autoMap
            (
                const fvPatchFieldMapper&
            );

            //- Reverse map the given fvPatchField onto this fvPatchField
            virtual void rmap
            (
                const fvPatchField<vector>&,
                const labelList&
            );


        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();
         
            //- Write
            virtual void write(Ostream&) const;




protected:
            virtual void updateU()=0;
            virtual void advectPoints(); 
            virtual void correctMass();
            virtual void initilise();
            virtual int numEddies();
            virtual void allocateSpots()=0;
            virtual void makeMirrorSpots( SEMspot* )=0; 

      
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
