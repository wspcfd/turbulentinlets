#include "DFSEMspot.H"
#include "turbulentInletDFSEMFvPatchField.H"


#include "utils.H"

namespace Foam
{



DFSEMspot::DFSEMspot
(
    SEMBase* pf 
)
: SEMspot( pf ),
alpha_(dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf )->qrMean2().size()),
qr_(dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf )->qrMean2().size()),
mapped_(dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf )->qrMean2().size())
{
    pf_ = pf;
}


DFSEMspot::DFSEMspot
(
    DFSEMspot* spt,
    SEMBase* pf 
)
:
    SEMspot( spt, pf ),
    alpha_(spt->alpha_),
    eigVals_(spt->eigVals_),
    eigVect_(spt->eigVect_), 
    qr_(spt->qr_),
    mapped_(spt->mapped_),
    best_(spt->best_)
{
    pf_ = pf;
}   


void DFSEMspot::interpolatePatchToSpot()
{
    SEMspot::interpolatePatchToSpot();

    eigVals_ = 
    ( donorProcN_ == Pstream::myProcNo() ) ?
    dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->eigVals()[nearest_]:
    pTraits<vector>::one * GREAT;

    eigVect_ = 
    ( donorProcN_ == Pstream::myProcNo() ) ?
    dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->eigVect()[nearest_]:
    pTraits<tensor>::one * GREAT;

    reduce( eigVals_, minOp<vector>() );
    reduce( eigVect_, minOp<tensor>() );


    this->setAlpha();

    //Sort eddy lengthscale
    if(sigma_.x()>sigma_.z())
    {
        Swap(sigma_.x(), sigma_.z());
    }
    
    if(sigma_.y()>sigma_.z())
    {
        Swap(sigma_.y(), sigma_.z());
    }

    if(sigma_.x()>sigma_.y())
    {
        Swap(sigma_.x(), sigma_.y());
    }
}


void DFSEMspot::setAlpha()
{
    List<Field<vector>* > qrMean2 = dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->qrMean2();
    List<Field<scalar>* > qMean2 = dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->qMean2();

    List<vector> A = dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->A();
 
    int numDivisions=qrMean2.size(); 

    for(int i=0; i<numDivisions; i++ )
    {
        if( pf_->newNormalisation() )
        {
            qr_[i] =  
            ( donorProcN_ == Pstream::myProcNo() ) ?
            vector
            ( 
                (*qrMean2[i])[nearest_] 
            ):
            pTraits<vector>::one * GREAT;

            reduce( qr_[i], minOp<vector>() );

            tensor cc = pTraits<vector>::one * qr_[i];
          
            cc = tensor
            (
                0.0, cc.xz(), cc.xy(),
                cc.yz(), 0.0, cc.yx(),
                cc.zy(), cc.zx(), 0.0
            );


            vector explicitStresses = 
            ( donorProcN_ == Pstream::myProcNo() ) ?
            vector
            (
                (*dynamic_cast<turbulentInletDFSEMFvPatchField*>( pf_ )->explicitStresses()[i])[nearest_]
            ):
            pTraits<vector>::one * GREAT;

            reduce( explicitStresses, minOp<vector>() ); 

            vector targetStress
            ( 
                max( eigVals_[0] - explicitStresses[0], 0.0 ),
                max( eigVals_[1] - explicitStresses[1], 0.0 ),
                max( eigVals_[2] - explicitStresses[2], 0.0 )
            );

            targetStress = vector( cmptDivide( targetStress + vector(SMALL, SMALL, SMALL), cmptMultiply(A[i], A[i]) ) );

            alpha_[i] = inv( cc ) & targetStress;
            if( donorProcN_ == Pstream::myProcNo() )
            {
                mapped_[i] = (cmptMin(alpha_[i]) < 0.0 ) ? false:true;
            }
            else
            {
                mapped_[i] = false;
            }
            reduce( mapped_[i], orOp<bool>() );
            reduce( alpha_[i], maxOp<vector>() ); 
        }
        else
        {
        }

        if( mapped_[i] )
        {
            alpha_[i] = cmptPow( alpha_[i], 0.5 );
        }
        else
        {
            alpha_[i] = pTraits<vector>::zero;
        }
    }

    if( pf_->newNormalisation() )
    {
        if( donorProcN_ == Pstream::myProcNo() )
        { 
            scalar sumQ = 0.0;

            //normalise alpha
            forAll( qMean2, i )
            {
                if( mapped_[i] )
                {
                    sumQ += pow( (*qMean2[i])[nearest_], 0.5 );
                }
            }
            forAll( qMean2, i )
            {
                alpha_[i] *= pow( (*qMean2[i])[nearest_], 0.5 ) / (sumQ+SMALL);
            }
        }
        else
        {
            forAll( qMean2, i )
            {
                alpha_[i] = vector( GREAT, GREAT, GREAT );
            }
        }

        forAll( qMean2, i )
        {
            reduce( alpha_[i], minOp<vector>() );
        }
    }

}

void DFSEMspot::projectBack( scalar delta, vector nn )
{
    if( donorProcN_ == Pstream::myProcNo() )
    {
        bool offPatch;
        do
        {
            origin_.x() += delta*nn.x();

            offPatch = true;
            forAll( pf_->patch().Cf(), facei )
            {
                vector distance = pf_->patch().Cf()[facei] - origin_;

                vector rk = cmptDivide
                ( 
                    distance & eigVect_.T(), 
                    sigma_
                );

                if( mag( rk ) < 1 )
                {
                    offPatch = false;
                    break;
                }
            }

        }while(!offPatch);
    }
    else
    {
        origin_ = pTraits<vector>::one * GREAT;
    }
    
    reduce( origin_, minOp<vector>() ); 
}



}
